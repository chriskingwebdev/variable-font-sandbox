# Variable Font Sandbox

A quick project I put togther to learn about variable fonts.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To run

In the project directory, run:

```
yarn install
yarn start
```

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Should look like this: 

![Variable Font Sandbox](./screenshot.png "Variable Font Sandbox")

## View a live version of the sandbox here:

[https://variable-font-sandbox.chriskingwebdev.com/](https://variable-font-sandbox.chriskingwebdev.com/)

## View a presentation about variable fonts here:

[https://variable-font-prezzo.chriskingwebdev.com/](https://variable-font-prezzo.chriskingwebdev.com/)