import React, { useState } from "react";

import "./App.css";
import Sliders from "./components/Sliders";
import Dropdowns from "./components/Dropdowns";
import { exampleText, fontData } from "./fontData";

const getDefaultFontSettings = (font) => {
    const fontValues = {};
    font.axes.forEach(axis => fontValues[axis.tag] = axis.default || 0);
    return fontValues;
};

function App() {
    const [state, setState] = useState({
        selectedFont: 0,
        fontSettings: getDefaultFontSettings(fontData[0]),
        selectedInstance: 0,
        selectedExampleText: 0
    });

    const fontSettings = state.fontSettings;
    const selectedFont = state.selectedFont;
    const selectedExampleText = state.selectedExampleText;

    const fontValueChanged = (axisName, newValue) => {
        const newFontSettings = {...fontSettings, ...{[axisName]: newValue} };
        setState({ ...state, fontSettings: newFontSettings });
    };

    const selectedFontData = fontData[selectedFont];

    return <div className="container">
        <h1 style={{
            fontFamily: `${selectedFontData.name}`,
            fontVariationSettings:
                `${selectedFontData.axes.map(
                    axis => `'${axis.tag}' ${fontSettings[axis.tag]}`)
                }` // "'INLN' 400, 'SWRM' 1000"
        }} >{ exampleText[selectedExampleText].value }</h1>
        <Dropdowns
            state={state}
            setState={setState}
            selectedFontData={selectedFontData}
            getDefaultFontSettings={getDefaultFontSettings}
        />
        <Sliders
            selectedFontData={selectedFontData}
            fontSettings={fontSettings}
            fontValueChanged={fontValueChanged}
        />
    </div>;
}

export default App;
