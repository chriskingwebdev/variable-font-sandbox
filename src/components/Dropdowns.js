import React from "react";
import Dropdown from "react-dropdown";

import { exampleText, fontData } from "../fontData";

const getFontSettingsFromInstance = (instance) => {
    const fontValues = {};
    Object.keys(instance.coordinates).forEach(coordinate => fontValues[coordinate] = instance.coordinates[coordinate] || 0);
    console.log(fontValues);
    return fontValues;
};

const Dropdowns = ({ state, setState, selectedFontData, getDefaultFontSettings }) => {
    const selectedFont = state.selectedFont;
    const selectedInstance = state.selectedInstance;
    const selectedExampleText = state.selectedExampleText;
    const instances = selectedFontData.instances;

    return <div className="dropdowns">
        <div className="dropdown">
            <label>Font Family</label>
            <Dropdown
                value={{ value: selectedFont, label: selectedFontData.name }}
                options={fontData.map((font, i) => ({ value: i, label: font.name,  }))}
                onChange={(e) => {
                    setState({
                        ...state,
                        selectedFont: e.value,
                        fontSettings: getDefaultFontSettings(fontData[e.value]),
                        selectedInstance: 0
                    });
                }}
            />
        </div>
        { instances && <div className="dropdown">
            <label>Instances</label>
            <Dropdown
                value={{ value: selectedInstance, label: instances[selectedInstance].name }}
                options={instances.map((instance, i) => ({ value: i, label: instance.name,  }))}
                onChange={(e) => {
                    setState({
                        ...state,
                        selectedInstance: e.value,
                        fontSettings: getFontSettingsFromInstance(instances[e.value]),
                    });
                }}
            />
        </div>}
        <div className="dropdown">
            <label>Example Text</label>
            <Dropdown
                value={{ value: selectedExampleText, label: exampleText[selectedExampleText].name }}
                options={exampleText.map((text, i) => ({ value: i, label: text.name  }))}
                onChange={(e) => {
                    setState({
                        ...state,
                        selectedExampleText: e.value
                    });
                }}
            />
        </div>
    </div>;

};
export default Dropdowns;