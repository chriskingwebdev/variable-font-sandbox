import React from "react";
import debounce from  "debounce";

const sliderChanged = debounce(({value, axis, onValueChange}) => {
    onValueChange(axis.tag, value);
}, 5);

const renderSliders = (font, fontSettings, onValueChange) => {
    return <>
        { font.axes.map((axis => {
            return <div key={axis.name} className="sliderRow">
                <label>{axis.name}</label>
                <span className="min">{axis.min || 0}</span>
                <input
                    type="range"
                    id={axis.name}
                    name={axis.name}
                    value={fontSettings[axis.tag]}
                    min={axis.min || 0}
                    max={ axis.max || 1000 }
                    onChange={ e => sliderChanged({value: e.target.value, axis, onValueChange})}
                    step={0.01}
                />
                <span className="max">{ axis.max || 1000 }</span>
                <span className="value">{fontSettings[axis.tag]}</span>
            </div>;
        })) }
    </>;
};

const Sliders = (props) => {
    const { selectedFontData, fontSettings, fontValueChanged } = props;

    return <div className="sliders">
        { renderSliders(selectedFontData, fontSettings, fontValueChanged) }
    </div>;
};
export default Sliders;