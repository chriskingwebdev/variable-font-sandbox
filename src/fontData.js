export const exampleText = [
    {
        name: "Grumpy Wizards",
        value: "Grumpy wizards make toxic brew for the evil Queen and Jack"
    },
    {
        name: "Quick Brown Fox",
        value: "The quick brown fox jumps over the lazy dog"
    },
    {
        name: "Lorem Ipsum",
        value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, ultrices aliquet neque tincidunt."
    }
];

export const fontData = [
    {
        name: "Aktiv Grotesk VF",
        axes: [
            { name: "Weight", tag: "wght", default: 300, min: 100, max: 900, },
            { name: "Width", tag: "wdth", default: 75, min: 75, max: 125 },
            { name: "Italic", tag: "ital", min: 0, max: 1 }
        ],
        instances: [
            {
                "name": "Hair Cd",
                "coordinates": {
                    "wght": 100,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "Hair Cd Italic",
                "coordinates": {
                    "wght": 100,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "Hair",
                "coordinates": {
                    "wght": 100,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "Hair Italic",
                "coordinates": {
                    "wght": 100,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "Hair Ex",
                "coordinates": {
                    "wght": 100,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "Hair Ex Italic",
                "coordinates": {
                    "wght": 100,
                    "wdth": 125,
                    "ital": 1
                }
            },{
                "name": "Thin Cd",
                "coordinates": {
                    "wght": 200,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "Thin Cd Italic",
                "coordinates": {
                    "wght": 200,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "Thin",
                "coordinates": {
                    "wght": 200,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "Thin Italic",
                "coordinates": {
                    "wght": 200,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "Thin Ex",
                "coordinates": {
                    "wght": 200,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "Thin Ex Italic",
                "coordinates": {
                    "wght": 200,
                    "wdth": 125,
                    "ital": 1
                }
            },{
                "name": "Light Cd",
                "coordinates": {
                    "wght": 300,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "Light Cd Italic",
                "coordinates": {
                    "wght": 300,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "Light",
                "coordinates": {
                    "wght": 300,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "Light Italic",
                "coordinates": {
                    "wght": 300,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "Light Ex",
                "coordinates": {
                    "wght": 300,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "Light Ex Italic",
                "coordinates": {
                    "wght": 300,
                    "wdth": 125,
                    "ital": 1
                }
            },{
                "name": "Cd",
                "coordinates": {
                    "wght": 400,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "Cd Italic",
                "coordinates": {
                    "wght": 400,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "Regular",
                "coordinates": {
                    "wght": 400,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "Italic",
                "coordinates": {
                    "wght": 400,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "Ex",
                "coordinates": {
                    "wght": 400,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "Ex Italic",
                "coordinates": {
                    "wght": 400,
                    "wdth": 125,
                    "ital": 1
                }
            },{
                "name": "Medium Cd",
                "coordinates": {
                    "wght": 500,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "Medium Cd Italic",
                "coordinates": {
                    "wght": 500,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "Medium",
                "coordinates": {
                    "wght": 500,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "Medium Italic",
                "coordinates": {
                    "wght": 500,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "Medium Ex",
                "coordinates": {
                    "wght": 500,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "Medium Ex Italic",
                "coordinates": {
                    "wght": 500,
                    "wdth": 125,
                    "ital": 1
                }
            },{
                "name": "Bold Cd",
                "coordinates": {
                    "wght": 700,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "Bold Cd Italic",
                "coordinates": {
                    "wght": 700,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "Bold",
                "coordinates": {
                    "wght": 700,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "Bold Italic",
                "coordinates": {
                    "wght": 700,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "Bold Ex",
                "coordinates": {
                    "wght": 700,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "Bold Ex Italic",
                "coordinates": {
                    "wght": 700,
                    "wdth": 125,
                    "ital": 1
                }
            },{
                "name": "XBold Cd",
                "coordinates": {
                    "wght": 800,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "XBold Cd Italic",
                "coordinates": {
                    "wght": 800,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "XBold",
                "coordinates": {
                    "wght": 800,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "XBold Italic",
                "coordinates": {
                    "wght": 800,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "XBold Ex",
                "coordinates": {
                    "wght": 800,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "XBold Ex Italic",
                "coordinates": {
                    "wght": 800,
                    "wdth": 125,
                    "ital": 1
                }
            },{
                "name": "Black Cd",
                "coordinates": {
                    "wght": 900,
                    "wdth": 75,
                    "ital": 0
                }
            },{
                "name": "Black Cd Italic",
                "coordinates": {
                    "wght": 900,
                    "wdth": 75,
                    "ital": 1
                }
            },{
                "name": "Black",
                "coordinates": {
                    "wght": 900,
                    "wdth": 100,
                    "ital": 0
                }
            },{
                "name": "Black Italic",
                "coordinates": {
                    "wght": 900,
                    "wdth": 100,
                    "ital": 1
                }
            },{
                "name": "Black Ex",
                "coordinates": {
                    "wght": 900,
                    "wdth": 125,
                    "ital": 0
                }
            },{
                "name": "Black Ex Italic",
                "coordinates": {
                    "wght": 900,
                    "wdth": 125,
                    "ital": 1
                }
            }
        ]
    },
    {
        name: "Rhododendron",
        axes: [
            { name: "Weight", tag: "wght", default: 300, min: 300, max: 800, },
            { name: "Descenders", tag: "DESC", max: 300 },
            { name: "Ascenders", tag: "ASCN", max: 300 }
        ],
        instances: [
            {
                "name": "Light",
                "coordinates": {
                    "DESC": 0,
                    "ASCN": 0,
                    "wght": 300
                }
            },{
                "name": "Regular",
                "coordinates": {
                    "DESC": 0,
                    "ASCN": 0,
                    "wght": 400
                }
            },{
                "name": "Medium",
                "coordinates": {
                    "DESC": 0,
                    "ASCN": 0,
                    "wght": 500
                }
            },{
                "name": "Bold",
                "coordinates": {
                    "DESC": 0,
                    "ASCN": 0,
                    "wght": 612.5
                }
            },{
                "name": "Black",
                "coordinates": {
                    "DESC": 0,
                    "ASCN": 0,
                    "wght": 800
                }
            }
        ]
    },
    {
        name: "Cheee",
        axes: [
            { name: "Yeast", tag: "yest" },
            { name: "Gravity", tag: "grvt" },
            { name: "Temperature", tag: "temp" }
        ]
    },
    {
        name: "Roboto Flex",
        axes: [
            { name: "wght", tag: "wght", min: 100, default: 400, max: 1000 },
            { name: "wdth", tag: "wdth", min: 25, default: 100, max: 151 },
            { name: "opsz", tag: "opsz", min: 8, default: 14, max: 144 },
            { name: "GRAD", tag: "GRAD", min: -1, default: 0, max: 1 },
            { name: "slnt", tag: "slnt", min: -10, default: 0, max: 0 },
            { name: "XTRA", tag: "XTRA", min: 323, default: 468, max: 603 },
            { name: "XOPQ", tag: "XOPQ", min: 27, default: 96, max: 175 },
            { name: "YOPQ", tag: "YOPQ", min: 25, default: 79, max: 135 },
            { name: "YTLC", tag: "YTLC", min: 416, default: 514, max: 570 },
            { name: "YTUC", tag: "YTUC", min: 528, default: 712, max: 760 },
            { name: "YTAS", tag: "YTAS", min: 649, default: 750, max: 854 },
            { name: "YTDE", tag: "YTDE", min: -305, default: -203, max: -98 },
            { name: "YTFI", tag: "YTFI", min: 560, default: 738, max: 788 }
        ]
    },
    {
        name: "Decovar Regular24",
        axes: [
            { name: "Inline", tag : "INLN" },
            { name: "Sheared", tag : "TSHR" },
            { name: "Rounded Slab", tag : "TRSB" },
            { name: "Stripes", tag : "SSTR" },
            { name: "Worm Terminal", tag : "TWRM" },
            { name: "Inline Skeleton", tag : "SINL" },
            { name: "Open Inline Terminal", tag : "TOIL" },
            { name: "Inline Terminal", tag : "TINL" },
            { name: "Worm", tag : "WORM" },
            { name: "Weight", tag : "wght", min :400, default:400},
            { name: "Flared", tag : "TFLR" },
            { name: "Rounded", tag : "TRND" },
            { name: "Worm Skeleton", tag : "SWRM" },
            { name: "Slab", tag : "TSLB" },
            { name: "Bifurcated", tag : "TBIF" }
        ],
        instances: [
            {
                "name": "Regular",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Open",
                "coordinates": {
                    "INLN": 1000,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Worm",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 1000,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Checkered",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 1000,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Checkered Reverse",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 1000,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Striped",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 500,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Rounded",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 1000,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Flared",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 1000,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Flared Open",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 1000,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 1000,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Rounded Slab",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 1000,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Sheared",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 1000,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Bifurcated",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 1000
                }
            },
            {
                "name": "Inline",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 500,
                    "TOIL": 500,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Slab",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 400,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 1000,
                    "TBIF": 0
                }
            },
            {
                "name": "Contrast",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 0,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 1000,
                    "TFLR": 0,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Fancy",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 0,
                    "SSTR": 0,
                    "TWRM": 0,
                    "SINL": 1000,
                    "TOIL": 0,
                    "TINL": 0,
                    "WORM": 0,
                    "wght": 1000,
                    "TFLR": 1000,
                    "TRND": 0,
                    "SWRM": 0,
                    "TSLB": 0,
                    "TBIF": 0
                }
            },
            {
                "name": "Mayhem",
                "coordinates": {
                    "INLN": 0,
                    "TSHR": 0,
                    "TRSB": 750,
                    "SSTR": 0,
                    "TWRM": 250,
                    "SINL": 1000,
                    "TOIL": 250,
                    "TINL": 250,
                    "WORM": 1000,
                    "wght": 750,
                    "TFLR": 500,
                    "TRND": 500,
                    "SWRM": 1000,
                    "TSLB": 750,
                    "TBIF": 500
                }
            }
        ]
    },
];